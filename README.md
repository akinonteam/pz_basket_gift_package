# pz_basket_gift_package
A library module for enabling "Gift Package and Note" or only "Gift Note" for products at basket page.
## Usage
### 1. Install the app

At project root, create a requirements.txt if it doesn't exist and add the following line to it;

```bash
# For latest version
-e git+https://git@bitbucket.org/akinonteam/pz_basket_gift_package.git#egg=pz_basket_gift_package

# For specific version (Recommended)
-e git+https://git@bitbucket.org/akinonteam/pz_basket_gift_package.git@35dcaaf#egg=pz_basket_gift_package
```

For more information about this syntax, check [pip install docs](https://pip.pypa.io/en/stable/reference/pip_install/#git).

Next, run the following command to install the package.

```bash
# in venv
pip install -r requirements.txt
```
### 2. Install the npm package

```bash
# in /templates

# For latest version
yarn add ​git+ssh://git@bitbucket.org:akinonteam/pz_basket_gift_package.git

# For specific version (Recommended)
yarn add ​git+ssh://git@bitbucket.org:akinonteam/pz_basket_gift_package.git#35dcaaf
```

Make sure to use the same git commit id as in `requirements.txt`.

### 3. Add to the project

```python
# omnife_base/settings.py:
INSTALLED_APPS.append('pz_basket_gift_package')
```
### 4. Import template:

### 4.1. For classic (w/ actions and content) type:
```jinja
{# in templates/basket/basket-list/index.html #}
{% from 'pz_basket_gift_package/index.html' import GiftBoxActions %}
{% from 'pz_basket_gift_package/index.html' import GiftBoxContent %}

{# example using scenario from inside of basket item #}
<div class="basket-item">
  ...
  <div class="basket-item__content">
    ...
    {{ GiftBoxActions() }}
    ...
  </div>
  ...
  {{ GiftBoxContent() }}
  ...
</div>
```

### 4.2. For basic (w/ content) type:
```jinja
{# in templates/basket/basket-list/index.html #}
{% from 'pz_basket_gift_package/note.html' import GiftNoteContent %}

{# example using scenario from inside of basket item #}
<div class="basket-item">
  ...
  {{ GiftNoteContent() }}
  ...
</div>
```

### 5. Inject Middlewares (For only lower than v2.0.0)

> **⚠ WARNING:** If your project version 2.0.0 or higher skip this step.

```js
// in templates/layout/index.js
import PzBasketGiftPackage from 'pz_basket_gift_package'; // For classic type
import PzBasketGiftNote from 'pz_basket_gift_package/note'; // For basic type
...
// at end of sheet (after all HttpClient.use(...) funcions)
PzBasketGiftPackage.applyMiddlewares(HttpClient.middlewares); // For classic type
PzBasketGiftNote.applyMiddlewares(HttpClient.middlewares); // For basic type
```

### 6. Import and initialize JS

### 6.1. For classic (w/ actions and content) type:
```js
// in templates/basket/basket-list/index.js
import PzBasketGiftPackage from 'pz_basket_gift_package';

// example using scenario from inside of setProducts method
class BasketList {
  ...
  setProducts = (...) => {
    ...
    items.forEach(basketItem => {
      const template = ...; // pre-defined template variable
      this.productList.appendChild(template); // template added to dom
      ...
      new PzBasketGiftPackage(template, basketItem);
      ...
    });
    ...
  }
  ...
}
```

### 6.2. For basic (w/ content) type:
```js
// in templates/basket/basket-list/index.js
import PzBasketGiftNote from 'pz_basket_gift_package/note';

// example using scenario from inside of setProducts method
class BasketList {
  ...
  setProducts = (...) => {
    ...
    items.forEach(basketItem => {
      const template = ...; // pre-defined template variable
      this.productList.appendChild(template); // template added to dom
      ...
      new PzBasketGiftNote(template, basketItem);
      ...
    });
    ...
  }
  ...
}
```

### 7. Import styles:

### 7.1. For classic (w/ actions and content) type:
```scss
// in templates/basket/basket-list/index.scss
@import '~pz_basket_gift_package/';
```

### 7.2. For basic (w/ content) type:
```scss
// in templates/basket/basket-list/index.scss
@import '~pz_basket_gift_package/note';
```

## Customizing the Html (Jinja Macro)

### 1- GiftBoxActions
```jinja
{# Simple Using #}
{{ GiftBoxActions() }}

{# Passing Parameters (w/ Default Parameters) #}
{{ GiftBoxActions(
  activeInfoText = _('Gift Pack Added'),
  activeInfoIcon = 'pz-icon-giftbox',
  addPackageTriggerText = _('Add Gift Pack'),
  addPackageTriggerIcon = 'pz-icon-giftbox',
  addNoteTriggerText = _('Add Note'),
  editNoteTriggerText = _('Change Note'),
  removePackageTriggerText = _('Remove Gift Pack')
) }}
```

All of the following are optional parameters for the Jinja2 macro.  
- **activeInfoText**: (String / Gettext, *Optional*)  
- **activeInfoIcon**: (String / Boolean, *Optional*)  
- **addPackageTriggerText**: (String / Gettext, *Optional*)  
- **addPackageTriggerIcon**: (String / Boolean, *Optional*)  
- **addNoteTriggerText**: (String / Gettext, *Optional*)  
- **editNoteTriggerText**: (String / Gettext, *Optional*)  
- **removePackageTriggerText**: (String / Gettext, *Optional*)  

### 2- GiftBoxContent
```jinja
{# Simple Using #}
{{ GiftBoxContent() }}

{# Passing Parameters (w/ Default Parameters) #}
{{ GiftBoxContent(
  formTitle = _('Add Gift Note'),
  closeFormTriggerText = _('Close'),
  noteMinLength = 0,
  noteMaxLength = 160,
  notePlaceholder = _('You can leave empty this area. However it will be a gift package without note.'),
  characterCounterMessage = _('characters left'),
  removeNoteTriggerText = _('Remove Note'),
  submitFormTriggerText = _('SAVE')
) }}
```

### 3- GiftNoteContent
```jinja
{# Simple Using #}
{{ GiftNoteContent() }}

{# Passing Parameters (w/ Default Parameters) #}
{{ GiftNoteContent(
  formTitle = _('Add Gift Note'),
  noteMinLength = 0,
  noteMaxLength = 160,
  notePlaceholder = _('You can leave empty this area.'),
  characterCounterMessage = _('characters left'),
  removeNoteTriggerText = _('Remove Note'),
  submitFormTriggerText = _('SAVE')
) }}
```

All of the following are optional parameters for the Jinja2 macro.  
- **formTitle**: (String / Gettext, *Optional*)  
- **closeFormTriggerText**: (String / Gettext, *Optional*)  
- **noteMinLength**: (Integer, *Optional*)  
- **noteMaxLength**: (Integer, *Optional*)  
- **notePlaceholder**: (String / Gettext, *Optional*)  
- **characterCounterMessage**: (String / Gettext, *Optional*)  
- **removeNoteTriggerText**: (String / Gettext, *Optional*)  
- **submitFormTriggerText**: (String / Gettext, *Optional*)  

## JS settings
Pass an object, which will be taken as destructured parameters, to customize things on the JS class.  

```js
// Simple Using
new PzBasketGiftPackage(template, basketItem); // For classic type
new PzBasketGiftNote(template, basketItem); // For basic type

// Passing Parameters (w/ Default Parameters)
new PzBasketGiftPackage( // For classic type
  template,
  basketItem,
  {
    defaultMessage: 'NO-GIFT-NOTE',
    defaultNoteMinlength: 0,
    defaultNoteMaxlength: 255,
  },
);

new PzBasketGiftNote( // For basic type
  template,
  basketItem,
  {
    defaultMessage: 'NO-GIFT-NOTE',
    defaultNoteMinlength: 0,
    defaultNoteMaxlength: 255,
  },
);
```
- **template**: (Node, *Required*)  
- **basketItem**: (Object, *Required*)  
- **{ defaultMessage }**: (String, *Optional*)  
- **{ defaultNoteMinlength }**: (Integer, *Optional*)  
- **{ defaultNoteMaxlength }**: (Integer, *Optional*)  