import { BasketService, HttpClient } from 'pz-core';
import { store } from 'shop-packages';
import { setBasket } from 'shop-packages/redux/basket/reducer';

export default class PzBasketGiftPackage {
  constructor(
    template,
    basketItem,
    {
      defaultMessage = 'NO-GIFT-NOTE',
      defaultNoteMinlength = 0,
      defaultNoteMaxlength = 255,
    } = {},
  ) {
    this.settings = {
      defaultMessage,
      defaultNoteMinlength,
      defaultNoteMaxlength,
    };

    this.selectors = {
      productGiftBoxActions: '.js-basket-item-product-gift-box-actions',
      productGiftBoxActiveInfo: '.js-basket-item-product-gift-box-active-info',
      productGiftBoxAddTrigger: '.js-basket-item-product-gift-box-add-trigger',
      productGiftBoxNoteAddTrigger: '.js-basket-item-product-gift-box-note-add-trigger',
      productGiftBoxNoteEditTrigger: '.js-basket-item-product-gift-box-note-edit-trigger',
      productGiftBoxRemoveTrigger: '.js-basket-item-product-gift-box-remove-trigger',
      productGiftBoxContent: '.js-basket-item-product-gift-box-content',
      productGiftBoxForm: '.js-basket-item-product-gift-box-form',
      productGiftBoxFormCloseTrigger: '.js-basket-item-product-gift-box-form-close-trigger',
      productGiftBoxNote: '.js-basket-item-product-gift-box-note',
      productGiftBoxCounter: '.js-basket-item-product-gift-box-counter',
      productGiftBoxCounterCurrent: '.js-basket-item-product-gift-box-counter-current',
      productGiftBoxCounterMax: '.js-basket-item-product-gift-box-counter-max',
      productGiftBoxNoteRemoveTrigger: '.js-basket-item-product-gift-box-note-remove-trigger',
      productGiftBoxSubmit: '.js-basket-item-product-gift-box-submit',
    };

    this.initGiftBoxOperations(template, basketItem);
  }

  static applyMiddlewares = (middlewares = []) =>
    middlewares.forEach((middleware) => HttpClient.use(middleware));

  initGiftBoxOperations = (template, basketItem) => {
    // Inner Functions
    const giftBoxRequest = async (note) => {
      disableActions();
      
      const { data } = await BasketService.setGiftNote({ id: basketItem.id, gift_note: note });

      store.dispatch(setBasket(data));
    };

    const noteLengthIsValid = (length) => length <= noteMaxlength && length >= noteMinlength;

    const checkNoteLength = (input) => {
      const length = input.value.length;
      const isValid = noteLengthIsValid(length);

      elements.productGiftBoxCounterCurrent.innerHTML = length;
      elements.productGiftBoxCounter.classList.toggle(countValidClass, isValid);
      elements.productGiftBoxCounter.classList.toggle(countErrorClass, !isValid);
      elements.productGiftBoxSubmit.disabled = !isValid;
    };

    const disableActions = () => [
      elements.productGiftBoxAddTrigger,
      elements.productGiftBoxNoteAddTrigger,
      elements.productGiftBoxNoteEditTrigger,
      elements.productGiftBoxRemoveTrigger,
      elements.productGiftBoxFormCloseTrigger,
      elements.productGiftBoxNoteRemoveTrigger,
      elements.productGiftBoxSubmit,
    ].forEach((trigger) => trigger.setAttribute('disabled-action', true));

    // Elements
    const elements = {
      productGiftBoxActions: template.querySelector(this.selectors.productGiftBoxActions),
      productGiftBoxActiveInfo: template.querySelector(this.selectors.productGiftBoxActiveInfo),
      productGiftBoxAddTrigger: template.querySelector(this.selectors.productGiftBoxAddTrigger),
      productGiftBoxNoteAddTrigger: template.querySelector(this.selectors.productGiftBoxNoteAddTrigger),
      productGiftBoxNoteEditTrigger: template.querySelector(this.selectors.productGiftBoxNoteEditTrigger),
      productGiftBoxRemoveTrigger: template.querySelector(this.selectors.productGiftBoxRemoveTrigger),
      productGiftBoxContent: template.querySelector(this.selectors.productGiftBoxContent),
      productGiftBoxForm: template.querySelector(this.selectors.productGiftBoxForm),
      productGiftBoxFormCloseTrigger: template.querySelector(this.selectors.productGiftBoxFormCloseTrigger),
      productGiftBoxNote: template.querySelector(this.selectors.productGiftBoxNote),
      productGiftBoxCounter: template.querySelector(this.selectors.productGiftBoxCounter),
      productGiftBoxCounterCurrent: template.querySelector(this.selectors.productGiftBoxCounterCurrent),
      productGiftBoxCounterMax: template.querySelector(this.selectors.productGiftBoxCounterMax),
      productGiftBoxNoteRemoveTrigger: template.querySelector(this.selectors.productGiftBoxNoteRemoveTrigger),
      productGiftBoxSubmit: template.querySelector(this.selectors.productGiftBoxSubmit),
    };

    // Main Declarations
    const giftNote = basketItem.attributes?.gift_note || null;
    const isDefaultNote = giftNote === this.settings.defaultMessage;
    const noteMinlength = elements.productGiftBoxNote.validationMinlength || this.settings.defaultNoteMinlength;
    const noteMaxlength = elements.productGiftBoxNote.validationMaxlength || this.settings.defaultNoteMaxlength;
    const countValidClass = '-valid-count';
    const countErrorClass = '-invalid-count';

    // Main Controls
    elements.productGiftBoxActions.hidden = false;
    elements.productGiftBoxContent.hidden = true;
    elements.productGiftBoxActiveInfo.hidden = !giftNote;
    elements.productGiftBoxAddTrigger.hidden = !!giftNote;
    elements.productGiftBoxNoteAddTrigger.hidden = !giftNote || !isDefaultNote;
    elements.productGiftBoxNoteEditTrigger.hidden = !giftNote || !!isDefaultNote;
    elements.productGiftBoxRemoveTrigger.hidden = !giftNote;
    elements.productGiftBoxNote.value = isDefaultNote ? '' : giftNote;
    elements.productGiftBoxNoteRemoveTrigger.hidden = !giftNote || !!isDefaultNote;
    elements.productGiftBoxCounterMax.innerHTML = noteMaxlength;
    checkNoteLength(elements.productGiftBoxNote);

    // Click Events
    elements.productGiftBoxAddTrigger.addEventListener('click', async () =>
      await giftBoxRequest(this.settings.defaultMessage));

    elements.productGiftBoxRemoveTrigger.addEventListener('click', async () =>
      await giftBoxRequest(null));

    [elements.productGiftBoxNoteAddTrigger, elements.productGiftBoxNoteEditTrigger, elements.productGiftBoxFormCloseTrigger]
      .forEach((trigger) =>
        trigger.addEventListener('click', () =>
          elements.productGiftBoxContent.hidden = !elements.productGiftBoxContent.hidden));

    elements.productGiftBoxNoteRemoveTrigger.addEventListener('click', async () =>
      await giftBoxRequest(this.settings.defaultMessage));

    // Form Events
    elements.productGiftBoxForm.beforeAction = (params) => {
      if (!noteLengthIsValid(params.gift_note.length)) {
        return false;
      }

      disableActions();

      params.id = basketItem.id;
      params.gift_note = params.gift_note || this.settings.defaultMessage;
    };

    elements.productGiftBoxForm.addEventListener('action-success', ({ detail }) =>
      store.dispatch(setBasket(detail.response.data)));

    // Misc Events
    [
      'change',
      'paste',
      'keyup',
      'blur',
      'focus'
    ].forEach((event) => elements.productGiftBoxNote.addEventListener(event, (e) => checkNoteLength(e.target)));
  }
}
